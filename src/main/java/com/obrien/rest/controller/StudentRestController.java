package com.obrien.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.obrien.rest.entity.Student;
import com.obrien.rest.exception.StudentErrorResponse;
import com.obrien.rest.exception.StudentNotFoundException;

@RestController
@RequestMapping("/api")
public class StudentRestController {
	
	private List<Student> students;
	
	@GetMapping("/students")
	public List<Student> getStudents(){
		return students;
	}
	
	@GetMapping("/students/{studentId}")
	public Student getStudentById(@PathVariable int studentId) {
		
		if(studentId < 0 || studentId >= students.size()) {
			throw new StudentNotFoundException("StudentID is not found " + studentId);
		}
		return students.get(studentId);
	}
	
	@PostConstruct
	public void populateStudents() {
		students = new ArrayList<>();
		students.add(new Student("Bob","Murphy"));
		students.add(new Student("Mary","Smith"));
		students.add(new Student("Bill","Kelly"));
		
	}
}
